﻿using NUnit.Framework;
using GitPractice.PageObjectModel;

namespace GitPractice.Tests
{
    [TestFixture]
    public class Login : BaseTest
    {
        [Test]
        public void GivenAnUserNameAndPassword_WhenSigningIn_ThenReturnAccountName()
        {
            HRMApp.LoginPage.RoleButton.Click();
            HRMApp.LoginPage.RoleNames[2].Click();
            HRMApp.LoginPage.WaitForMenuProfile();

            Assert.IsFalse(string.IsNullOrEmpty(HRMApp.LoginPage.AccountName.Text));
        }
    }
}
