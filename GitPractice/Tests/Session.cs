﻿using NUnit.Framework;
using GitPractice.PageObjectModel;
using System.Threading;

namespace GitPractice.Tests
{
    public class Session:BaseTest
    {
        private const string sessionName = "Practicing Automation";
        private const string frameId = "noncoreIframe";
        private const string empId = "0059";
        private const string stdId = "0057";
        private const string inValidEmpId = "KG";
        private const int trainerTabNumber = 1;
        private const int particpantTabNumber = 2;

        [SetUp]
        public void Setup()
        {
            HRMApp.LoginPage.RoleButton.Click();
            HRMApp.LoginPage.RoleNames[0].Click();
            HRMApp.LoginPage.WaitForMenuProfile();
            HRMApp.TrainingPage.TrainingLink.Click();
            HRMApp.TrainingPage.SessionLink.Click();
            HRMApp.SessionPage.WaitForSessionContent();
            HRMApp.SessionPage.ChangeIframe(frameId);
            HRMApp.SessionPage.WaitForIframe();
            HRMApp.SessionPage.AddBtns[0].Click();
            HRMApp.SessionPage.WaitForAddSessionPage();
        }

        [Test]
        public void GivenValidInputs_WhenAddingSession_ThenDisplaySuccessMessage()
        {
            HRMApp.SessionPage.SessionName.SendKeys(sessionName);
            HRMApp.SessionPage.CourseName.Click();
            HRMApp.SessionPage.WaitForCourseDropDownOptions();
            HRMApp.SessionPage.CourseDropDownOptions[1].Click();
            HRMApp.SessionPage.StartDate.Click();
            HRMApp.SessionPage.TodayBtn.Click();
            HRMApp.SessionPage.SaveBtn.Click();
            HRMApp.SessionPage.WaitForSuccessMessage();

            Assert.True(HRMApp.SessionPage.SuccessMessage.Displayed);
        }
        
        [Test]
        public void GivenInValidInputs_WhenAddingSession_ThenDisplayValidationMessage()
        {
            HRMApp.SessionPage.SaveBtn.Click();
            HRMApp.SessionPage.WaitForValidationMessages();

            Assert.True(HRMApp.SessionPage.NameValidationMessage.Displayed);
        }
        
        [Test]
        public void GivenValidInputs_WhenAddingTrainer_ThenDisplaySuccessMessage()
        {
            HRMApp.SessionPage.NavigateToTab(trainerTabNumber);
            HRMApp.SessionPage.AddBtns[0].Click(); ;
            HRMApp.SessionPage.WaitForAddTrainerModalToDisplay();
            HRMApp.SessionPage.TrainerName.SendKeys(empId);
            HRMApp.SessionPage.NamesList[0].Click();
            HRMApp.SessionPage.AddTrainerBtn.Click();
            HRMApp.SessionPage.WaitForAddTrainerModalToDispose();

            Assert.IsFalse(HRMApp.SessionPage.TrainersModal.Displayed);
        }
        
        [Test]
        public void GivenInValidInputs_WhenAddingTrainer_ThenDisplayValidationMessage()
        {
            HRMApp.SessionPage.NavigateToTab(trainerTabNumber);
            HRMApp.SessionPage.AddBtns[0].Click(); ;
            HRMApp.SessionPage.TrainerName.SendKeys(inValidEmpId);
            HRMApp.SessionPage.WaitForAddTrainerModalToDisplay();
            HRMApp.SessionPage.AddTrainerBtn.Click();

            Assert.IsTrue(HRMApp.SessionPage.TrainerNameValidationMessage.Displayed);
        }

        [Test]
        public void GivenValidInputs_WhenAddingParticipant_ThenDisplaySuccessMessage()
        {
            HRMApp.SessionPage.NavigateToTab(particpantTabNumber);
            HRMApp.SessionPage.AddBtns[1].Click(); ;
            HRMApp.SessionPage.WaitForAddParticipantModalToDisplay();
            HRMApp.SessionPage.StudentName.SendKeys(stdId);
            HRMApp.SessionPage.NamesList[0].Click();
            HRMApp.SessionPage.AddStudentBtn.Click();
            HRMApp.SessionPage.WaitForAddParticipantModalToDispose();

            Assert.IsFalse(HRMApp.SessionPage.ParticipantModal.Displayed);
        }
        
        [Test]
        public void GivenInValidInputs_WhenAddingParticipant_ThenDisplaySuccessMessage()
        {
            HRMApp.SessionPage.NavigateToTab(particpantTabNumber);
            HRMApp.SessionPage.AddBtns[1].Click(); ;
            HRMApp.SessionPage.WaitForAddParticipantModalToDisplay();
            HRMApp.SessionPage.AddStudentBtn.Click();

            Assert.IsTrue(HRMApp.SessionPage.StudentNameValidationMessage.Displayed);
        }
    }
}
