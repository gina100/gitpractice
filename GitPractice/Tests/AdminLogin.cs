﻿using GitPractice.PageObjectModel;
using NUnit.Framework;

namespace GitPractice.Tests
{
    [TestFixture]
    public class AdminLogin : BaseTest
    {
        [SetUp]
        public void LoginAsAdminUser()
        {
            HRMApp.LoginPage.LoginButton.Click();
        }
    }
}
