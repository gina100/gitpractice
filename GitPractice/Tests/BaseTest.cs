﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;
using GitPractice.PageObjectModel;

namespace GitPractice.Tests
{
    [TestFixture]
    public class BaseTest
    {
        private  IWebDriver driver;
        private const int driverWaitTime = 15;
        private const string maximizeWindow = "--start-maximized";

        [SetUp]
        public void Setup()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments(maximizeWindow);
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), chromeOptions);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(driverWaitTime);
            HRMApp.Init(driver);
        }

        [TearDown]
        public void CleanUp()
        {
            driver.Dispose();
        }
    }
}
