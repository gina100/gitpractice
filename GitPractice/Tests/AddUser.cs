﻿using GitPractice.PageObjectModel;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitPractice.Tests
{
    [TestFixture]
    public class AddUser : AdminLogin
    {
        [SetUp]
        public void OpenAddUserModal()
        {
            HRMApp.BasePage.AdminLink.Click();
            HRMApp.BasePage.UserManagementLink.Click();
            HRMApp.BasePage.UsersLink.Click();
            HRMApp.UserPage.WaitForUserPage();
            HRMApp.UserPage.AddUserButton.Click();
            HRMApp.UserPage.WaitforAddUserModal();
        }

        [Test]
        public void GivenEmptyField_WhenAddingUser_ThenConfirmValidationMessageIsDisplayed()
        {
            HRMApp.UserPage.EmployeeNameField.SendKeys(string.Empty);
            HRMApp.UserPage.EmployeeUsername.SendKeys(string.Empty);
            HRMApp.UserPage.SaveButton.Click();

            Assert.AreEqual(4, HRMApp.UserPage.RequiredMessage.Count);
        }

        [Test]
        public void GivenValidInformation_WhenAddingUser_ThenConfirmSuccessMessage()
        {
            HRMApp.UserPage.EmployeeNameField.SendKeys("Aaliyah Haq");
            HRMApp.UserPage.EmployeeUsername.SendKeys("Haq Da King");
            HRMApp.UserPage.PasswordField.SendKeys("Happy@King");
            HRMApp.UserPage.ConfirmPasswordField.SendKeys("Happy@King");
            HRMApp.UserPage.SaveButton.Click();

            Assert.IsTrue(HRMApp.UserPage.SaveSuccessMessage.Displayed);
        }

    }
}
