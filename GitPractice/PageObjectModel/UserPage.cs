﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections;
using System.Collections.Generic;

namespace GitPractice.PageObjectModel
{
    public class UserPage : BasePage
    {
        public UserPage(IWebDriver driver) : base(driver)
        {

        }

        private IWebElement UserContainer => Driver.FindElement(By.XPath("//list//table"));

        private IWebElement AddUserModal => Driver.FindElement(By.Id("modal1"));

        public IWebElement AddUserButton => Driver.FindElement(By.XPath("//a[.//i[text()='add']]"));

        public IList<IWebElement> RequiredMessage => Driver.FindElements(By.Id("//*[text()='Required']"));

        public IWebElement EmployeeNameField => Driver.FindElement(By.Id("selectedEmployee_value"));

        public IWebElement EmployeeUsername => Driver.FindElement(By.Id("user_name"));

        public IWebElement PasswordField => Driver.FindElement(By.Id("password"));

        public IWebElement ConfirmPasswordField => Driver.FindElement(By.Id("confirmpassword"));

        public IWebElement SaveButton => Driver.FindElement(By.Id("systemUserSaveBtn"));

        public IWebElement SaveSuccessMessage => Driver.FindElement(By.XPath("//*[text()='Successfully Saved']"));

        public void WaitForUserPage()
        {
            Wait.Until(result => UserContainer.Displayed);
        }

        public void WaitforAddUserModal()
        {
            Wait.Until(result => AddUserModal.Displayed);
        }
    }
}
