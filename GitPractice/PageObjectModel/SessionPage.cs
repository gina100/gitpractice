﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace GitPractice.PageObjectModel
{
    public class SessionPage:TrainingPage
    {
        public SessionPage(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement addSessionPage => PageRoot.FindElement(By.Id("addSession"));
        private IWebElement sessionMenu => PageRoot.FindElement(By.Id("add_session_left_menu"));
        public IWebElement TrainersModal => PageRoot.FindElement(By.Id("addTrainersModal"));
        public IWebElement ParticipantModal => PageRoot.FindElement(By.Id("addStudent"));
        public IWebElement SessionName => addSessionPage.FindElement(By.Id("addSession_name"));
        public IWebElement CourseName => addSessionPage.FindElement(By.CssSelector("input.select-dropdown"));
        public IList<IWebElement> CourseDropDownOptions => addSessionPage.FindElements(By.CssSelector("div.select-wrapper li"));
        public IWebElement StartDate => addSessionPage.FindElement(By.Id("addSession_scheduledDate"));
        public IWebElement TodayBtn => addSessionPage.FindElement(By.CssSelector("#addSession_scheduledDate_root button"));
        public IWebElement SaveBtn => addSessionPage.FindElement(By.CssSelector("a#btnSave"));
        public IWebElement SuccessMessage => addSessionPage.FindElement(By.CssSelector("div.toast-message"));
        public IWebElement NameValidationMessage => addSessionPage.FindElement(By.Id("addSession_name-error"));
        public IWebElement CourseValidationMessage => addSessionPage.FindElement(By.Id("addSession_course-error"));
        public IWebElement StartDateValidationMessage => addSessionPage.FindElement(By.Id("addSession_scheduledDate-error"));
        public IList<IWebElement> SessionTabs => sessionMenu.FindElements(By.CssSelector("li"));
        public IWebElement TrainerName => TrainersModal.FindElement(By.Id("defineSessionTrainer_trainer_empName"));
        public IWebElement StudentName => ParticipantModal.FindElement(By.Id("defineSessionStudent_student_empName"));
        public IWebElement AddTrainerBtn => TrainersModal.FindElement(By.Id("btnEmployeeAdd"));
        public IWebElement AddStudentBtn => ParticipantModal.FindElement(By.Id("btnAddStudentToSession"));
        public IWebElement TrainerNameValidationMessage => TrainersModal.FindElement(By.Id("defineSessionTrainer_trainer_empName-error"));
        public IWebElement StudentNameValidationMessage => ParticipantModal.FindElement(By.Id("defineSessionStudent_student_empName-error"));
        public IList<IWebElement> NamesList => PageRoot.FindElements(By.CssSelector("div.ac_results li"));

        public void WaitForAddSessionPage()
        {
            Wait.Until(driver => addSessionPage.Displayed);
        }

        public void NavigateToTab(int tabNumber)
        {
            SessionTabs[tabNumber].Click();
        }
        
        public void WaitForAddTrainerModalToDisplay()
        {
            Wait.Until(driver => TrainersModal.Displayed);
        }

        public void WaitForAddTrainerModalToDispose()
        {
            Wait.Until(driver => !TrainersModal.Displayed);
        }

        public void WaitForAddParticipantModalToDisplay()
        {
            Wait.Until(driver => ParticipantModal.Displayed);
        }

        public void WaitForAddParticipantModalToDispose()
        {
            Wait.Until(driver => !ParticipantModal.Displayed);
        }

        public void WaitForCourseDropDownOptions()
        {
            Wait.Until(driver => CourseDropDownOptions.Count>0);
        }

        public void WaitForSuccessMessage()
        {
            Wait.Until(driver => SuccessMessage.Displayed);
        }
        public void WaitForValidationMessages()
        {
            Wait.Until(driver => NameValidationMessage.Displayed);
            Wait.Until(driver => CourseValidationMessage.Displayed);
            Wait.Until(driver => StartDateValidationMessage.Displayed);
        }
    }
}
