﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace GitPractice.PageObjectModel
{
    public class BasePage
    {
        private const int waitTime = 30;
        public IWebDriver Driver { get; set; }
        public WebDriverWait Wait { get; set; }


        public BasePage(IWebDriver driver)
        {
            Wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitTime));
            this.Driver = driver;
        }
        
        public IWebElement PageRoot => Driver.FindElement(By.CssSelector("body"));

        public IWebElement AdminLink => PageRoot.FindElement(By.Id("menu_admin_viewAdminModule"));

        public IWebElement UserManagementLink => AdminLink.FindElement(By.Id("menu_admin_UserManagement"));

        public IWebElement UsersLink => UserManagementLink.FindElement(By.Id("menu_admin_viewSystemUsers"));
    }
}
