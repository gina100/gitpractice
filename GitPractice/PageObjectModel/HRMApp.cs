﻿using OpenQA.Selenium;

namespace GitPractice.PageObjectModel
{
    public static class HRMApp
    {
        public static LoginPage LoginPage;
        public static SessionPage SessionPage;
        public static TrainingPage TrainingPage;
        public static UserPage UserPage;
        public static BasePage BasePage;

        public static void Init(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("https://orangehrm-demo-6x.orangehrmlive.com/");
            LoginPage = new LoginPage(driver);
            SessionPage = new SessionPage(driver);
            TrainingPage = new TrainingPage(driver);
            UserPage = new UserPage(driver);
            BasePage = new BasePage(driver);
        }
    }
}
