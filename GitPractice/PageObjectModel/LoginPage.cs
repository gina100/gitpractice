﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace GitPractice.PageObjectModel
{
    public class LoginPage : BasePage
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {

        }

        public IWebElement  RoleButton => PageRoot.FindElement(By.Id("loginAsButtonGroup"));

        public IWebElement AccountName => PageRoot.FindElement(By.Id("account-name"));
        
        public IList<IWebElement> RoleNames => RoleButton.FindElements(By.CssSelector("a"));
        
        private IWebElement menuProfile => PageRoot.FindElement(By.Id("menu-profile"));

        public IWebElement LoginButton => Driver.FindElement(By.Id("btnLogin"));

        public void WaitForMenuProfile()
        {
            Wait.Until(driver => menuProfile.Displayed);
        }
    }
}
