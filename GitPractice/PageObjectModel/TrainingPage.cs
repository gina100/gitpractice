﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace GitPractice.PageObjectModel
{
    public class TrainingPage:BasePage
    {
        public TrainingPage(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement TrainingLink => PageRoot.FindElement(By.CssSelector("#menu_training_defaultTrainingModulePage a"));
        public IWebElement SessionLink => PageRoot.FindElement(By.CssSelector("a#menu_training_viewSessionList"));
        public IWebElement SessionContent=> Driver.FindElement(By.Id("content"));
        public IWebElement SessionList => Driver.FindElement(By.Id("TestList"));
        public IList<IWebElement> AddBtns => PageRoot.FindElements(By.CssSelector("a#addItemBtn"));

        public void WaitForSessionContent()
        {
            Wait.Until(driver=>SessionContent.Displayed);
        }

        public void ChangeIframe(string frameId)
        {
            Driver.SwitchTo().Frame(frameId);
        }

        public void WaitForIframe()
        {
            Wait.Until(driver => SessionList);
        }
    }
}
